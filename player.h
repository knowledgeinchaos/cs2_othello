#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

struct BoardMoves {

    BoardMoves *parent;
    BoardMoves *firstKid;
    BoardMoves *next;
    BoardMoves *prev; 

    int score;
    int updatedScore;
    Board *board;
    int level;
    Side side; 
    int x;
    int y;

    BoardMoves(BoardMoves *parent, int level, int score, Board *board,
                    Side side, int x, int y);
    ~BoardMoves();

    void doMove(int x, int y);
    void addKids();
    void addNext(BoardMoves *next);

    void prune();
    void deleteNext(); 
    void deleteKids(); 
}; 




class Player {

private:
    Board *board; 
    Side side; 
    Side other; 

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);


    void setBoard(char data[]); 

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
