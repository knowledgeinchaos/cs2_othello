
The readme for my code for the CS2 Othello assignement.

There were 2 main improvements made:
    1) Rather than passing around a Move object internelly,
the xy coordinates are used in the Board and the Player. This
reduces the memory allocation needed (since one would need to
create a new move whenever it is used, and memory allocation is 
slow). 
    It's not like my thing takes very long anyway, but eh.

    2) Each valid move has a weighting, based on the sum of 
the weighted scores of the squares flipped as a result of
enacting the move. 
    This makes things a little better, but the numbers
are kind of arbitrary. (It does mean that I can avoid the
squares adjacent to the corners like the plague though.)

    If I had more time for this assignement, I'd probably look/generate
a good opening book, with the opening boards stored as a hashtable. 
    I'd probably try to implement an end-game solver, to maximize for
the last 10 moves or so. (By number of overall pieces claimed.)
    Of course, there is also the min-max tree with alpha-beta pruning
that would probably be good to implement at some point. 

    Alas, I've got other classes I need to finish. 

    Here's an ASCII frog, because why not. 

     /     \
     _(I)(I)_
    ( _ .. _ )
     `.`--'.'
      )    (
  ,-./      \,-.
 ( _( ||  || )_ )
__\ \\||--||'/ /__
`-._//||\/||\\_.-'
     `--'`--'






















