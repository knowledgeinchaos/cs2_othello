#include "player.h"

#include <iostream>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    this->side = side; 

    if (side == WHITE) this->other = BLACK;
    else this->other = WHITE; 

    board = new Board(); 

}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board; 
}

/*
 * Sets the input board to some new board.
 */
void Player::setBoard(char data[]) {
    board->setBoard(data); 

}




/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    if (opponentsMove != NULL)
        board->doMove(opponentsMove->getX(), opponentsMove->getY(), other);

    int score = MAGIC_INVALID; 
    int X;
    int Y;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            if (board->checkMove(i, j, side)) { 
                int temp = board->getMoveValue(i, j, side);
                if (score < temp) {
                    score = temp;
                    X = i;
                    Y = j;
                }
           }
        }
    }
    if (score != MAGIC_INVALID) {
        board->doMove(X, Y, side);
        return new Move(X, Y);
    }
     return NULL; 
/*
    BoardMoves * b = new BoardMoves(NULL, 0, 0, this->board.copy(),
                        side, MAGIC_INVALID, MAGIC_INVALID);
    // add 2 levels of kids
    b->addKids();  

    BoardMoves *k = b->firstKid;
    while (k != NULL) {
        k->addKids();
    }

    b->prune();
    
    int resx = b->firstKid->x;
    int resy = b->firstKid->y;

    b->deleteNext();

    if (resx == MAGIC_INVALID) return NULL;
    return (new Move(resx, resy));
*/
}


/* Constructor for Board Moves
 */
BoardMoves::BoardMoves(BoardMoves *parent, int level, int score,
        Board *board, Side side, int x, int y) {
    this->parent = parent;
    this->level = level;
    
    this->firstKid = NULL;
    this->next = NULL;
    this->prev = NULL;
    this->score = score;
    
    this->board = board;
    this->x = x;
    this->y = y;
    
    this->side = side; 
    this->updatedScore = MAGIC_INVALID;
}




/* Destructor. Does delete children */
BoardMoves::~BoardMoves() {
    delete this->board;
    firstKid->deleteNext(); 
}

/* Does the move to the board. Doesn't change anything else */
void BoardMoves::doMove(int x, int y) {
    board->doMove(x, y, side);
}

/* Adds child BoardMoves */
void BoardMoves::addKids() {
    int temp;
    int nextScore;
    Side nextSide = (this->side == BLACK)? WHITE : BLACK;
    BoardMoves *prevKid = NULL;
    BoardMoves *nextKid;
    for (int i = 0; i < 8; i++) {
        for (int j =0; j < 8; j++) {
            // see if the move is valid
            temp = board->getMoveValue(x, y, nextSide);
            if (temp != MAGIC_INVALID) {
                // if so, add a new kid with the score
                if (level % 2 == 0) {
                    nextScore = score + temp;
                }
                else nextScore = score - temp;

                nextKid = new BoardMoves(this, 
                        level + 1, nextScore, board->copy(), nextSide, i, j);
                nextKid->doMove(x, y);

                if (prevKid == NULL) {
                    firstKid = nextKid;
                }
                else {
                    prevKid->addNext(nextKid);
                }
                prevKid = nextKid;
            }
        }
    }

    if (firstKid == NULL) { // looks like didn't actually manage to 
                            // find a valid move. Future functions
                            // will still expect a kid though
        if (level % 2 == 0) {
            firstKid = new BoardMoves(this, level + 1, 
                MAGIC_INVALID, board->copy(), nextSide,
                 MAGIC_INVALID, MAGIC_INVALID);
        }
        else firstKid = new BoardMoves(this, level + 1,
                MAGIC_INVALID * -1, board->copy(), nextSide,
                 MAGIC_INVALID, MAGIC_INVALID);
    }

}

/* Adds next BoardMove to current one. */
void BoardMoves::addNext(BoardMoves *next) {
    this->next = next;
    next->prev = this;
}

/* Deletes this element and the next one and all associated kids*/
void BoardMoves::deleteNext() {    
    if (next != NULL) {
        next->deleteNext();
    }
    deleteKids();
    delete this;
}

/* Deletes the kids of this element */
void BoardMoves::deleteKids() {
    if (firstKid != NULL) {
        firstKid->deleteNext();
    }
}

/* Updates score of current element based on that of kid elements.
 * This deletes all non-maximal/minimal kids.
 * Also deletes all non-maximal neighbors  */
void BoardMoves::prune() {
    if (firstKid != NULL) {
        firstKid->prune();
        score += firstKid->score; 
    }

    if (next != NULL) {
        next->prune();
        if (level % 2) { // even level, so want minimal score
            if (next->score < this->score) {
                if (prev == NULL) {
                    parent->firstKid = next;
                }
                else prev->addNext(next);
                deleteKids();
                delete this;
                return;
            }
            else {
                BoardMoves *tempNext = next->next;
                next->deleteKids();
                delete next;
                next = tempNext;
            }
        }
        else { // odd level, want maximal score    
            if (next->score > this->score) {
                if (prev == NULL) {
                    parent->firstKid = next;
                }
                else prev->addNext(next);
                deleteKids();
                delete this;
                return;
            }
            else {
                BoardMoves *tempNext = next->next;
                next->deleteKids();
                delete next;
                next = tempNext;
            }
         }
    }

}    
    
