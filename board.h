#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(int x, int y, Side side);
    void doMove(int x, int y, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    int getPoints(int x, int y); 
    int getMoveValue(int x, int y, Side side);


    void setBoard(char data[]);
};

const int MAGIC_INVALID = -99999; // ought to be smaller than any potential num

// weightings for squares
const int CORNER = 100;
const int DANGER = -1;
const int SIDES = 50;
const int NORMAL = 1; 

#endif
